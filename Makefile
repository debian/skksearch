SRC = main.c err.c dic.c dic_dummy.c dic_plain.c dic_cdb.c dic_db.c
OBJ = main.o err.o dic.o dic_dummy.o dic_plain.o dic_cdb.o dic_db.o

CC = gcc
CFLAGS = -g -DSYSLOG -I../cdb-0.55 -I/usr/local/BerkeleyDB/include # -DDEBUG_FROM_TERMINAL=1
LDFLAGS = -L../cdb-0.55 -lcdb -L/usr/local/BerkeleyDB/lib -ldb


default all: skksearch

skksearch: $(OBJ)
	$(CC) $(CFLAGS) -o $@ $(OBJ) $(LDFLAGS)

clean:
	rm -f *.o *~ core skksearch a.out
