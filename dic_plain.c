/*
 * Copyright (c) 1999 Hideki Sakurada
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "config.h"
#include "err.h"
#include "dic.h"
#include "dic_plain.h"

#ifndef SEEK_SET
#define SEEK_SET 0
#endif /* SEEK_SET */
#ifndef SEEK_END
#define SEEK_END 2
#endif /* SEEK_END */


#define OKURI_A_STR ";; okuri-ari entries.\n"
#define OKURI_N_STR ";; okuri-nasi entries.\n"

int dic_plain_compare(char *, char *, int, int);


struct dic *dic_plain_open(struct dic *d, char *path) {
  struct dic_plain_internal *internal;
  long offset;
  char buf[DIC_BUFSIZE];

  if ((internal = malloc(sizeof(struct dic_plain_internal))) == NULL) {
    err(LOG_ERR, "dic_plain_open: %s\n", strerror(errno));
    exit(1);
  }

  if ((internal->fp = fopen(path, "r")) == NULL) {
    err(LOG_ERR, "dic_plain_open: %s\n", strerror(errno));
    exit(1);
  }

  /* search OKURI_A_STR */
  fseek(internal->fp, 0, SEEK_SET);

  while(fgets(buf, DIC_BUFSIZE, internal->fp)) {
    if (strcmp(buf, OKURI_A_STR) == 0) {
      internal->okuri_a_begin = ftell(internal->fp);
      goto found_a;
    }
  }

  fprintf(stderr, "cannot find okuri_a_str in dic %s\n", path);
  exit(1);

found_a:

  /* search OKURI_N_STR */
  while(offset = ftell(internal->fp), fgets(buf, DIC_BUFSIZE, internal->fp)) {
    if (strcmp(buf, OKURI_N_STR) == 0) {
      internal->okuri_a_end = offset;
      internal->okuri_n_begin = ftell(internal->fp);
      goto found_n;
    }
  }

  fprintf(stderr, "cannot find okuri_n_str in dic %s\n", path);
  exit(1);

found_n:

  fseek(internal->fp, 0, SEEK_END);
  internal->okuri_n_end = ftell(internal->fp);

  d->internal = (void *)internal;
  d->search = dic_plain_search;
  d->close = dic_plain_close;

  return d;
}


char *dic_plain_search(struct dic *d, char *key, int keylen) {
  int okuri_ari;
  long begin;
  long end;
  long half;
  struct dic_plain_internal *internal
    = (struct dic_plain_internal *)(d->internal);
  FILE *fp = internal->fp;
  char *buf = d->buf;

  /* okuri check */
  if (key[keylen - 2] & 0x80
      && 'a' <= key[keylen - 1] && key[keylen - 1] <= 'z') {
    /* okuri ari */
    okuri_ari = 1;
    begin = internal->okuri_a_begin;
    end = internal->okuri_a_end;
  } else {
    /* okuri nashi */
    okuri_ari = 0;
    begin = internal->okuri_n_begin;
    end = internal->okuri_n_end;
  }

  while (1) {
    fseek(fp, (end + begin) / 2, SEEK_SET); /* center */
    fgets(buf, DIC_BUFSIZE, fp); /* Skip */
    if ((half = ftell(fp)) >= end) {
      break;			/* goto linear search */
    }
    fgets(buf, DIC_BUFSIZE, fp);

    /* okuri ari entries are in reverse order */
    switch(dic_plain_compare(key, buf, keylen, okuri_ari)) {
    case 1:
      /* search the lower half */
      begin = ftell(fp);
      break;
    case -1:
      /* search the upper half */
      end = half;
      break;
    case 0:
      return buf + keylen + 1;
    default:
      err(LOG_ERR, "dic_plain_search: bad return from dic_plain_compare()\n");
      exit(1);
    }
  }
  /* linear search */
  fseek(fp, begin, SEEK_SET);
  while (ftell(fp) < end && fgets(buf, DIC_BUFSIZE, fp)) {
    switch (dic_plain_compare(key, buf, keylen, okuri_ari)) {
    case 1:
      /* continue */
      break;
    case -1:
      /* not found */
      return NULL;
    case 0:
      /* found */
      return buf + keylen + 1;
    default:
      err(LOG_ERR, "invlid return value from compare()\n");
      exit(1);
    }
  }
  return NULL;
}


int dic_plain_close(struct dic *d) {
  struct dic_plain_internal *internal
    = (struct dic_plain_internal *)(d->internal);
  fclose(internal->fp);
  free(internal);
  free(d);
  return 0;
}


int dic_plain_compare(char *key, char *line, int keylen, int okuri_ari) {
  int comp;
  err(LOG_DEBUG,
      "dic_plain_compare:\n\tkey=\"%*s\"\n\tline=\"%s\"\n", keylen, key, line);
  comp = (okuri_ari ? -1 : 1) * strncmp(key, line, keylen);
  if (comp > 0) {
    err(LOG_DEBUG, "\tvalue=%d\n", 1);
    return 1;
  } else if (comp < 0) {
    err(LOG_DEBUG, "\tvalue=%d\n", -1);
    return -1;
  } else {			/* comp == 0 */
    if (strlen(line) <= keylen) {
      /* invalid */
      err(LOG_ERR, "dic_plain_compare: invalid line %s", line);
      exit(1);
    }
    if (line[keylen] != ' ') {
      err(LOG_DEBUG, "\tvalue=%d\n", -1);
      return -1;
    }
    err(LOG_DEBUG, "\tvalue=%d\n", 0);
    return 0;
  }
}
