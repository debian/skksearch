/*
 * Copyright (c) 1999 Hideki Sakurada
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <stdarg.h>
#include "err.h"

#define DEFAULT_LOGLEVEL 4;

/* -1(disabled), 0(the lowest) - 7(the highest) */
int loglevel  = DEFAULT_LOGLEVEL;
int use_syslog;
char *logfile;

static FILE *errout;


void openerr() {
  if (logfile != NULL)
    {
      errout = fopen(logfile, "a");
    }
#ifdef SYSLOG
  else
    if (use_syslog) {
      openlog(SYSLOG_IDENT, LOG_PID, SYSLOG_FACILITY);
    }
#endif /* SYSLOG */
  return;
}


void closeerr() {
  if (errout != NULL)
    fclose(errout);
#ifdef SYSLOG
  else if (use_syslog)
    closelog();
#endif /* SYSLOG */
}


void err(int priority, char *message, ...) {
  va_list argp;

  va_start(argp, message);

  if (errout != NULL) {
    if (loglevel >= priority)
      vfprintf(errout, message, argp);
  }
#ifdef SYSLOG
  else
    if (use_syslog) vsyslog(priority, message, argp);
#endif /* SYSLOG */
  va_end(argp);
}
