/*
 * Copyright (c) 1999 Hideki Sakurada
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "config.h"

#ifdef SYSLOG
#include <syslog.h>
#endif /* SYSLOG */
#include "err.h"

#include "dic.h"

#define STDIN 0
#define STDOUT 1

#define COMBUFSIZE 1024
char combuf[COMBUFSIZE];

#define CLIENT_END       '0'
#define CLIENT_REQUEST   '1'
#define CLIENT_VERSION   '2'
#define CLIENT_HOST      '3'

#define SERVER_ERROR     '0'
#define SERVER_FOUND     '1'
#define SERVER_NOT_FOUND '4'
#define SERVER_FULL      '9'

char *version_string = "0.0";

/* declared in err.c */
extern int loglevel;
extern int use_syslog;
extern char *logfile;

/* dic */
#define MAXDICNUM 100
struct dic *diclist[MAXDICNUM];
int dicnum;

#define MAXENTNUM 1024
char *entlist[MAXENTNUM];
int entnum;
int inentry(char *);
void searchdicts();


int main(int argc, char *argv[]) {
  int c;
  int s;
  int i;
  int errflag = 0;
  int terminal = 0;
  char *conf = NULL;
  extern char *optarg;
  extern int optind;

  /* process arguments */
  while ((c = getopt(argc, argv, "l:f:sic:t")) != EOF) {
   switch (c) {
    case 'l':
      loglevel = atoi(optarg);
      break;
    case 'f':
      logfile = optarg;
      break;
    case 's':
      use_syslog = 1;
      break;
    case 't':
      terminal = 1;
    case 'c':
      conf = optarg;
      break;
    case '?':
      errflag = 1;
    }
  }
  if (errflag) {
    fprintf(stderr,
	    "usage: %s [-l digit] [-f logfile] [-s] [-c config] [dictinary1 ...] \n",
	    argv[0]);
    exit(1);
  }

  /* open logfile or syslog */
  openerr();
  err(LOG_INFO, "%s started\n", argv[0]);

  /* open dictionaries */
  if ((dicnum = argc - optind) > MAXDICNUM) {
    err(LOG_ERR, "too many %d dicts\n", argc - optind);
    exit(1);
  }
  for (i = 0; i < dicnum; i++) {
    err(LOG_DEBUG, "opening dictionary %s", argv[optind + i]);
    diclist[i] = dic_open(argv[optind + i]);
  }
  if (conf != NULL) {
     FILE *fp;
     char line[1024];
     char *p;
     fp = fopen(conf, "r");
     if (fp == NULL) {
	err(LOG_ERR, "cannot open config file `%s'\n", conf);
	exit(1);
     }
     while ((p = fgets(line, sizeof(line), fp)) != NULL) {
	if (p[0] == '#' || p[0] == '\n') 
	    continue;
	p[strlen(p)-1] = '\0'; /* chop */
	if (i >= MAXDICNUM) {
	    err(LOG_ERR, "too many dicts in config file\n");
	    exit(1);
	}
	err(LOG_DEBUG, "opening dictionary %s", p);
	dicnum++;
	diclist[i++] = dic_open(p);
     }
     fclose(fp);
  }
  if (dicnum == 0) {
     err(LOG_ERR, "no dict specified\n");
     exit(1);
  }

  /* main loop */
#ifdef DEBUG_FROM_TERMINAL
  while (fgets(combuf, COMBUFSIZE, stdin) != NULL) {
#else
  while ((s = read(STDIN, combuf, COMBUFSIZE - 1)) > 0) {
    combuf[s] = '\0';
#endif
    switch (combuf[0]) {
    case CLIENT_END:
      goto end;
    case CLIENT_REQUEST:
      searchdicts();
      break;
    case CLIENT_VERSION:
      write(STDOUT, version_string, strlen(version_string));
      break;
    case CLIENT_HOST:		/* not yet XXX */
      write(STDOUT, "localhost:", strlen("localhost:"));
      break;
    }
  }

end:
  /* close dictionaries */
  for (i = 0; i < dicnum; i++) {
    diclist[i]->close(diclist[i]);
  }

  /* closing logfile or syslog */
  err(LOG_DEBUG, "closing log\n");
  closelog();

  exit(0);
}



void searchdicts(){
  int i;
  char *p;
  int keylen;
  char *key;

  /* initialize */
  entnum = 0;

  key = &combuf[1];

  if ((p = strchr(key, ' ')) == NULL) {
    goto output;
  }
  keylen = p - key;

  /* search */
  for (i = 0; i < dicnum; i++) {
    if ((p = diclist[i]->search(diclist[i], key, keylen)) != NULL) {
      if ((p = strtok(p, "/")) == NULL) {
	err(LOG_ERR, "searchdicts: invalid entry %*s\n", keylen, key);
	exit(1);
      }
      do {
	if (!inentry(p)) {
	  entlist[entnum++] = p;
	  if (entnum == MAXENTNUM) goto output;
	}
      } while ((p = strtok(NULL, "/\n")) != NULL);
    }
  }

  /* output */
output:
  if (entnum == 0) {
    combuf[0] = SERVER_NOT_FOUND;
    fputs(combuf, stdout);
  } else {
    putchar(SERVER_FOUND);
    for (i = 0; i < entnum; i++) {
      putchar('/');
      fputs(entlist[i], stdout);
    }
    fputs("/\n", stdout);
  }
  fflush(stdout);
}


int inentry(char *entry) {
  int i;
  for (i = 0; i < entnum; i++) {
    if (strcmp(entry, entlist[i]) == 0) {
      return 1;
    }
  }
  return 0;
}
