/*
 * Copyright (c) 1999 Hideki Sakurada
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <db.h>
#include "config.h"
#include "err.h"
#include "dic.h"
#include "dic_db.h"

/* BerkeleyDB environment */
int env_initialized = 0;
DB_ENV *env;
void errcall(const DB_ENV *, const char *, const char *);


struct dic *dic_db_open(struct dic *d, char *path) {
  int ret;
  struct dic_db_internal *internal;

  if (!env_initialized) {
    if ((ret = db_env_create(&env, 0)) != 0
	|| (ret = env->open(env, NULL, 0, DB_USE_ENVIRON)) != 0) {
      err(LOG_ERR, "dic_db_open: %s\n", db_strerror(ret));
      exit(1);
    }
    env->set_errcall(env, errcall);
    env_initialized = 1;
  }

  if ((internal = malloc(sizeof(struct dic_db_internal))) == NULL) {
    err(LOG_ERR, "dic_db_open(%s): %s\n", path, strerror(errno));
    exit(1);
  }
  memset(internal, 0, sizeof(struct dic_db_internal));

  if ((ret = db_create(&(internal->db), env, 0)) != 0
      || (ret = internal->db->open(internal->db, NULL, path, NULL,
				   DB_UNKNOWN, DB_RDONLY, 0)) != 0) {
    err(LOG_ERR, "dic_db_open(%s): %s\n", path, db_strerror(ret));
    exit(1);
  }
  d->internal = (void *)internal;
  d->search = dic_db_search;
  d->close = dic_db_close;
  return d;
  }

char *dic_db_search(struct dic *d, char *keystr, int keylen) {
  int ret;
  struct dic_db_internal *internal = (struct dic_db_internal *)(d->internal);
  DB *db = internal->db;
  DBT key;
  DBT data;

  key.data = keystr;
  key.size = keylen;
  key.flags = 0;
  data.data = d->buf;
  data.size = 0;
  data.ulen = DIC_BUFSIZE - 1;	/* -1 for '\0' */
  data.flags = DB_DBT_USERMEM;

  if ((ret = db->get(db, NULL, &key, &data, 0)) == DB_NOTFOUND) {
    return NULL;
  } else if (ret == 0) {	/* found */
    *((char *)(data.data) + data.size) = '\0';
    return (char *)(data.data);
  } else {
    err(LOG_WARNING, "dic_db_search: %s (may be too long entry)\n",
	db_strerror(ret));
    return NULL;
  }
}


int dic_db_close(struct dic *d) {
  int ret;
  DB *db = ((struct dic_db_internal *)(d->internal))->db;
  if ((ret = db->close(db, 0)) != 0) {
    err(LOG_ERR, "dic_db_close: %s\n", db_strerror(ret));
    exit(1);
  }
  free(d->internal);
  free(d);
  return 0;
}


void errcall(const DB_ENV *dbenv, const char *foo, const char *message) {
  err(LOG_ERR, "dic_db: %s\n", message);
}
