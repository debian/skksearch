/*
 * Copyright (c) 1999 Hideki Sakurada
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "config.h"
#include "err.h"
#include "dic.h"

#ifdef DIC_DUMMY
#include "dic_dummy.h"
#endif /* DIC_DUMMY */

#ifdef DIC_PLAIN
#include "dic_plain.h"
#endif /* DIC_PLAIN */

#ifdef DIC_CDB
#include "dic_cdb.h"
#endif /* DIC_CDB */

#ifdef DIC_DB
#include <db.h>
#include "dic_db.h"
#endif /* DIC_DB */


struct dic *dic_open(char *dicstr) {
  struct dic *d;

  if ((d = malloc(sizeof(struct dic))) == NULL) {
    err(LOG_ERR, "dic_open: %s\n", strerror(errno));
    exit(1);
  }

#ifdef DIC_DUMMY
  if (strncmp(dicstr, "dummy:", strlen("dummy:")) == 0) {
    d = (void *)dic_dummy_open(d, dicstr + strlen("dummy:"));
  }
#endif /* DIC_DUMMY */
#ifdef DIC_PLAIN
  else if (strncmp(dicstr, "plain:", strlen("plain:")) == 0) {
    d = (void *)dic_plain_open(d, dicstr + strlen("plain:"));
  }
#endif /* DIC_PLAIN */
#ifdef DIC_CDB
  else if (strncmp(dicstr, "cdb:", strlen("cdb:")) == 0) {
    d = (void *)dic_cdb_open(d, dicstr + strlen("cdb:"));
  }
#endif /* DIC_CDB */
#ifdef DIC_DB
  else if (strncmp(dicstr, "db:", strlen("db:")) == 0) {
    d = (void *)dic_db_open(d, dicstr + strlen("db:"));
  }
#endif /* DIC_DB */
  else {
    err(LOG_ERR, "dic_open: dictionary of unknown type %s\n", dicstr);
    exit(1);
  }

  return d;
}

