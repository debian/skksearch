/*
 * Copyright (c) 1999 Hideki Sakurada
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

struct dic_cdb_internal {
  int fd;
};

struct dic *dic_cdb_open(struct dic *, char *);
int dic_cdb_close(struct dic *);
char *dic_cdb_search(struct dic *, char *, int);
