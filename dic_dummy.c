/*
 * Copyright (c) 1999 Hideki Sakurada
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "config.h"
#include "err.h"
#include "dic.h"
#include "dic_dummy.h"

/* Dummy dictionary type is just a trivial example. */


struct dic *dic_dummy_open(struct dic *d, char *path) {
  struct dic_dummy_internal *internal;
  if ((internal = malloc(sizeof(struct dic_dummy_internal))) == NULL) {
    err(LOG_ERR, "dic_dummy_open: %s\n", strerror(errno));
    exit(1);
  }
  d->internal = (void *)internal;
  d->search = dic_dummy_search;
  d->close = dic_dummy_close;
  return d;
}


int dic_dummy_close(struct dic *d) {
  free(d->internal);
  free(d);
  return 0;
}


char *dic_dummy_search(struct dic *d, char *key, int keylen) {
  return NULL;
}
